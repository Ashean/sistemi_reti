package com.example.myshape;

import androidx.appcompat.app.AppCompatActivity;

import android.graphics.ColorFilter;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.widget.LinearLayout;
import android.widget.SeekBar;

public class MainActivity extends AppCompatActivity {

    LinearLayout llCerchio;
    SeekBar sbTrasparenza;
    SeekBar sbRed;
    SeekBar sbGreen;
    SeekBar sbBlue;
    int[] colorRGB = new int[3];

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        llCerchio = (LinearLayout) findViewById(R.id.llCerchio);
        sbTrasparenza = (SeekBar) findViewById(R.id.sbTrasparenza);
        sbRed = (SeekBar) findViewById(R.id.sbRed);
        sbGreen = (SeekBar) findViewById(R.id.sbGreen);
        sbBlue = (SeekBar) findViewById(R.id.sbBlue);
        sbTrasparenza.setOnSeekBarChangeListener(seekBarChangeListener);
        sbBlue.setOnSeekBarChangeListener(seekBarChangeListenerColor);
        sbGreen.setOnSeekBarChangeListener(seekBarChangeListenerColor);
        sbRed.setOnSeekBarChangeListener(seekBarChangeListenerColor);
    }

    private SeekBar.OnSeekBarChangeListener seekBarChangeListenerColor =
            new SeekBar.OnSeekBarChangeListener() {
                @Override
                public void onProgressChanged(SeekBar seekBar, int progress, boolean b) {

                    GradientDrawable myCircle = (GradientDrawable) llCerchio.getBackground();
                    updateBackground(myCircle);
                }

                @Override
                public void onStartTrackingTouch(SeekBar seekBar) {

                }

                @Override
                public void onStopTrackingTouch(SeekBar seekBar) {

                }
            };

    private SeekBar.OnSeekBarChangeListener seekBarChangeListener =
            new SeekBar.OnSeekBarChangeListener() {
                @Override
                public void onProgressChanged(SeekBar seekBar, int progress, boolean b) {

                    GradientDrawable myCircle = (GradientDrawable) llCerchio.getBackground();
                    myCircle.setAlpha(progress);

                }

                @Override
                public void onStartTrackingTouch(SeekBar seekBar) {

                }

                @Override
                public void onStopTrackingTouch(SeekBar seekBar) {

                }
            };

    private void updateBackground(GradientDrawable myCircle) {
        int seekR = sbRed.getProgress();
        int seekG = sbGreen.getProgress();
        int seekB = sbBlue.getProgress();
        myCircle.setColor(
                0xff000000
                        + seekR * 0x10000
                        + seekG * 0x100
                        + seekB
        );

    }
}