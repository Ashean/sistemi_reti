package com.example.mychangergb;

import androidx.appcompat.app.AppCompatActivity;

import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.EditText;

import org.w3c.dom.Text;

public class MainActivity extends AppCompatActivity {
    LinearLayout llCerchio;
    SeekBar sbTrasparenza;
    SeekBar sbRed;
    SeekBar sbGreen;
    SeekBar sbBlue;
    EditText tAlpha;
    EditText tRed;
    EditText tGreen;
    EditText tBlue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        llCerchio = (LinearLayout) findViewById(R.id.llCerchio);
        sbTrasparenza = (SeekBar) findViewById(R.id.sbTrasparenza);
        sbRed = (SeekBar) findViewById(R.id.sbRed);
        sbGreen = (SeekBar) findViewById(R.id.sbGreen);
        sbBlue = (SeekBar) findViewById(R.id.sbBlue);
        tAlpha = (EditText) findViewById(R.id.tAlpha);
         tRed =(EditText) findViewById(R.id.tRed);
         tGreen=(EditText) findViewById(R.id.tGreen);
         tBlue =(EditText) findViewById(R.id.tBlue);
        sbTrasparenza.setOnSeekBarChangeListener(seekBarChangeListener);
        sbBlue.setOnSeekBarChangeListener(seekBarChangeListenerColor(1));
        sbGreen.setOnSeekBarChangeListener(seekBarChangeListenerColor(0x100));
        sbRed.setOnSeekBarChangeListener(seekBarChangeListenerColor(0x1000));

    }
    
    private SeekBar.OnSeekBarChangeListener seekBarChangeListenerColor(int ASCII) =
            new SeekBar.OnSeekBarChangeListener() {
                @Override
                public void onProgressChanged(SeekBar seekBar, int progress, boolean b) {

                    GradientDrawable myCircle = (GradientDrawable) llCerchio.getBackground();
                    updateBackground(myCircle);
                }

                @Override
                public void onStartTrackingTouch(SeekBar seekBar) {

                }

                @Override
                public void onStopTrackingTouch(SeekBar seekBar) {

                }
            };

    private SeekBar.OnSeekBarChangeListener seekBarChangeListener =
            new SeekBar.OnSeekBarChangeListener() {
                @Override
                public void onProgressChanged(SeekBar seekBar, int progress, boolean b) {

                    GradientDrawable myCircle = (GradientDrawable) llCerchio.getBackground();
                    tAlpha.setText((CharSequence)(""+progress * 100 /255 + "%"));
                    myCircle.setAlpha(progress);

                }

                @Override
                public void onStartTrackingTouch(SeekBar seekBar) {

                }

                @Override
                public void onStopTrackingTouch(SeekBar seekBar) {

                }
            };

    private void updateBackground(GradientDrawable myCircle) {

        int seekR = sbRed.getProgress();
        tRed.setText((CharSequence) (""+sbRed.getProgress() * 100 /255 ));
        int seekG = sbGreen.getProgress();
        tGreen.setText((CharSequence)(""+sbGreen.getProgress() *100 /255 ));
        int seekB = sbBlue.getProgress();
        tBlue.setText((CharSequence)(""+sbBlue.getProgress() * 100 /255 ));

        myCircle.setColor(
                0xff000000
                        + (Integer.parseInt(tRed.getText().toString())/100*255)* 0x10000
                        + (Integer.parseInt(tGreen.getText().toString())/100*255) * 0x100
                        + (Integer.parseInt(tBlue.getText().toString())/100*255)
        );

    }
    }

