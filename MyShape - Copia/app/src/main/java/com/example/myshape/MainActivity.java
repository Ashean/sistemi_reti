package com.example.myshape;

import androidx.appcompat.app.AppCompatActivity;

import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.widget.LinearLayout;
import android.widget.SeekBar;

public class MainActivity extends AppCompatActivity {

    LinearLayout llCerchio;
    SeekBar sbTrasparenza;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        llCerchio = (LinearLayout) findViewById(R.id.llCerchio);
        sbTrasparenza = (SeekBar) findViewById(R.id.sbTrasparenza);

        sbTrasparenza.setOnSeekBarChangeListener(seekBarChangeListener);
    }
        private SeekBar.OnSeekBarChangeListener seekBarChangeListener=
                new SeekBar.OnSeekBarChangeListener() {
                    @Override
                    public void onProgressChanged(SeekBar seekBar, int progress, boolean b) {

                        GradientDrawable myCircle = (GradientDrawable) llCerchio.getBackground();
                        myCircle.setAlpha(progress);

                    }

                    @Override
                    public void onStartTrackingTouch(SeekBar seekBar) {

                    }

                    @Override
                    public void onStopTrackingTouch(SeekBar seekBar) {

                    }
                };


    }
