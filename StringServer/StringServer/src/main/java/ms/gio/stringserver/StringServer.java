/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ms.gio.stringserver;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author sergio
 */
public class StringServer {

    /*
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        String buf;
        
        try {
            // creo il server
            
            ServerSocket ss = new ServerSocket(9999);
            while(true) {
                // leggo i dati
                try ( // accetto connessioni
                        
                        Socket s = ss.accept()) {
                        // leggo i dati (un testo)
                        InputStreamReader isr;
                        isr = new InputStreamReader(s.getInputStream());
                        BufferedReader in = new BufferedReader(isr);
                        PrintWriter out = new PrintWriter(s.getOutputStream(), true);
                        // Invio una risposta
                        out.println("Ciao dal Server!");
                        do {
                            buf = in.readLine(); 
                            if (buf.toUpperCase().compareTo("QUIT") == 0)
                                break;
                            System.out.println("User: " + buf );
                            out.println("Server: " + buf.toUpperCase());
                            System.out.println("Server: " + buf.toUpperCase());
                        } while(true);
                        // chiudo la connessione e riparto
                        
                }  catch (Exception e) {
                    e.printStackTrace();
                }
            } // while
        } catch (IOException ex) {
            Logger.getLogger(StringServer.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
}
