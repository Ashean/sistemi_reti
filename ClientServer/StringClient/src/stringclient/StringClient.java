package stringclient;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Scanner;

/**
 *
 * @author Stefano Giacomello
 */
public class StringClient {
private static String alfabeto = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz ,.:!?"; 
private static String worm = "ser";
    public static void main(String[] args) throws IOException {

        // instanzia un socket per la connessione
        Socket s = new Socket("localhost", 8080);
        
        // invia dati al server
        PrintWriter out = new PrintWriter(s.getOutputStream(), true);
        out.println(criptaggio(alfabeto, worm, userInput()));

        // riceve una risposta dal server
        InputStreamReader isr = new InputStreamReader(s.getInputStream());
        BufferedReader in = new BufferedReader(isr);
        System.out.println(decriptaggio(alfabeto,worm, in.readLine()));

        s.close();
    }
    private static String readFromInput() {
		Scanner in = new Scanner(System.in);
		return in.nextLine();
	}

    
    public static String userInput(){
        String msg;
        System.out.println("----- Si inserisca il dato da inviare al server ----");
        msg = readFromInput();
        return msg;
    }
    
  
   public static String criptaggio(String alfabeto, String worm, String rispStr) {		
		String parolaCriptata = new String();
		for (int i = 0, j = 0; i < rispStr.length(); i++, j++) {
			if(j == worm.length()){
				j = 0;
			}			
			int v = (alfabeto.indexOf(rispStr.charAt(i))) + (alfabeto.indexOf(worm.charAt(j)));
			while (v > alfabeto.length()-1) {
				v = v - (alfabeto.length());
			}			
			parolaCriptata += alfabeto.charAt(v);
		}
		return parolaCriptata;
	}
   
	public static String decriptaggio(String alfabeto, String worm,String rispStr) {		
		String parolaDecriptata = new String();
		for (int i = 0, j = 0; i < rispStr.length(); i++, j++) {
			if(j == worm.length()){
				j = 0;
			}                       
			int v = (alfabeto.indexOf(rispStr.charAt(i))) - (alfabeto.indexOf(worm.charAt(j)));
			while (v < 0) {
				v = (alfabeto.length()) + v;
			}			
			parolaDecriptata += alfabeto.charAt(v);
                }
                return  parolaDecriptata;
	}	
}

