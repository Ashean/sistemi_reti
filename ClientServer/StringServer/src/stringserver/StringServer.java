package stringserver;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;

/**
 *
 * @author Stefano Giacomello
 */
public class StringServer {
 private static String alfabeto = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz ,.:!?"; 
private static String worm = "ser";

    public static void main(String[] args) throws IOException {

        // instanzia un server socket per la connessione
        ServerSocket ss = new ServerSocket(8080);

        while (true) {
            // riceve una connessione
            Socket s = ss.accept();

            // riceve del testo
            InputStreamReader isr = new InputStreamReader(s.getInputStream());
            BufferedReader in = new BufferedReader(isr);
            System.out.println(decriptaggio(alfabeto,worm, in.readLine()));

            // invia del testo
            PrintWriter out = new PrintWriter(s.getOutputStream(), true);
            out.println(criptaggio(alfabeto, worm, "dato ricevuto"));

            s.close();
        }

    }
    
     public static String criptaggio(String alfabeto, String worm, String rispStr) {		
		String parolaCriptata = new String();
		for (int i = 0, j = 0; i < rispStr.length(); i++, j++) {
			if(j == worm.length()){
				j = 0;
			}			
			int v = (alfabeto.indexOf(rispStr.charAt(i))) + (alfabeto.indexOf(worm.charAt(j)));
			while (v > alfabeto.length()-1) {
				v = v - (alfabeto.length());
			}			
			parolaCriptata += alfabeto.charAt(v);
		}
		return parolaCriptata;
	}
   
	public static String decriptaggio(String alfabeto, String worm,String rispStr) {		
		String parolaDecriptata = new String();
		for (int i = 0, j = 0; i < rispStr.length(); i++, j++) {
			if(j == worm.length()){
				j = 0;
			}                       
			int v = (alfabeto.indexOf(rispStr.charAt(i))) - (alfabeto.indexOf(worm.charAt(j)));
			while (v < 0) {
				v = (alfabeto.length()) + v;
			}			
			parolaDecriptata += alfabeto.charAt(v);
                }
                return  parolaDecriptata;
	}	
}
