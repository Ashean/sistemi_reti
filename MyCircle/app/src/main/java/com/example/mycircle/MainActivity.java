package com.example.mycircle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.LinearLayoutCompat;

import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.widget.LinearLayout;
import android.widget.SeekBar;

public class MainActivity extends AppCompatActivity {

    LinearLayout llCerchio;
    SeekBar sbTasparenza;

    @Override
    //funzione che parte durante il built del progamma
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //collego gli ogetti grafci al codice
        llCerchio = (LinearLayout) findViewById(R.id.llCerchio);
        sbTasparenza = (SeekBar) findViewById(R.id.sbTrasparenza);

        //aggiungo un event listener ad un oggetto
        sbTasparenza.setOnSeekBarChangeListener(seekBarChangeListener);
    }

    /*creo una funzione ascoltatore*/
    private SeekBar.OnSeekBarChangeListener seekBarChangeListener = new SeekBar.OnSeekBarChangeListener() {
        @Override
        public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

            GradientDrawable myCircle = (GradientDrawable) llCerchio.getBackground();   //ho preso l' oggetto backgroud di un oggetto
            myCircle.setAlpha(progress);
        }

        @Override
        public void onStartTrackingTouch(SeekBar seekBar) {

        }

        @Override
        public void onStopTrackingTouch(SeekBar seekBar) {

        }


    };

}

