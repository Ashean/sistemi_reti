package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.content.Intent;

public class SubActivity extends AppCompatActivity  {
    Button btnSub;
    TextView tbRecievedText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        btnSub = (Button) findViewById(R.id.btnSub) ;
        tbRecievedText = (TextView) findViewById(R.id.tb_sub) ;
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sub);

        btnSub.setOnClickListener(btnGoMain);
        tbRecievedText.setText(getIntent().getExtras().getString("content"));
    }



    public final Button.OnClickListener btnGoMain =
            new Button.OnClickListener() {


                @Override
                public void onClick(View v) {
                    startActivity();
                }
            };

    public void startActivity(){
        Intent MainActivity = new Intent(this, MainActivity.class);
        startActivityForResult(MainActivity, 2);
    }
}
