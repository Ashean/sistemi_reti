package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.content.Intent;

public class MainActivity extends AppCompatActivity {
    Button btnMain;
    TextView tbMain;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        btnMain = (Button) findViewById(R.id.btnSub) ;
        tbMain = (TextView) findViewById(R.id.tb_main) ;
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btnMain.setOnClickListener(btnGoToSub);
    }

    public final Button.OnClickListener btnGoToSub =
            new Button.OnClickListener() {


                @Override
                public void onClick(View v) {
                    startActivity();
                }
            };

         public void startActivity(){
             Intent subActivity = new Intent(this, SubActivity.class);

             subActivity.putExtra("content", tbMain.getText().toString());
             startActivityForResult(subActivity, 1);
         }
}
